export interface NFTAttribute {
  trait_type: string;
  value: string;
}

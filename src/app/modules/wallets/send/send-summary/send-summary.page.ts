import { Component, OnInit } from '@angular/core';
import { TransactionDataService } from '../../shared-wallets/services/transaction-data/transaction-data.service';
import { SummaryData } from './interfaces/summary-data.interface';
import { SubmitButtonService } from '../../../../shared/services/submit-button/submit-button.service';
import { WalletTransactionsService } from '../../shared-wallets/services/wallet-transactions/wallet-transactions.service';
import { AlertController, ModalController, NavController } from '@ionic/angular';
import { WalletPasswordComponent } from '../../shared-wallets/components/wallet-password/wallet-password.component';
import { ActivatedRoute } from '@angular/router';
import { LoadingService } from 'src/app/shared/services/loading/loading.service';
import { LocalNotificationsService } from '../../../notifications/shared-notifications/services/local-notifications/local-notifications.service';
import { TransactionReceipt, TransactionResponse } from '@ethersproject/abstract-provider';
import { TranslateService } from '@ngx-translate/core';
import { LocalNotificationSchema } from '@capacitor/local-notifications';

@Component({
  selector: 'app-send-summary',
  template: ` <ion-header>
      <ion-toolbar color="uxprimary" class="ux_toolbar">
        <ion-buttons slot="start">
          <ion-back-button defaultHref="/wallets/home"></ion-back-button>
        </ion-buttons>
        <ion-title class="ion-text-center">{{ 'wallets.send.send_summary.header' | translate }}</ion-title>
      </ion-toolbar>
    </ion-header>
    <ion-content class="ss ion-padding">
      <div class="ss__title">
        <ion-text class="ux-font-text-lg">
          {{ 'wallets.send.send_summary.title' | translate }}
        </ion-text>
      </div>
      <div class="ss__transaction-summary-card" *ngIf="this.summaryData">
        <app-transaction-summary-card
          [amountsTitle]="'wallets.send.send_summary.amounts_title' | translate"
          [summaryData]="this.summaryData"
        ></app-transaction-summary-card>
      </div>

      <div class="ss__send_button">
        <ion-button
          class="ux_button"
          color="uxsecondary"
          appTrackClick
          name="Send"
          [disabled]="(this.submitButtonService.isDisabled | async) || this.isSending"
          (click)="this.canAffordFee()"
          >{{ 'wallets.send.send_summary.send_button' | translate }}</ion-button
        >
      </div>
    </ion-content>`,
  styleUrls: ['./send-summary.page.scss'],
})
export class SendSummaryPage implements OnInit {
  summaryData: SummaryData;
  action: string;
  isSending: boolean;
  constructor(
    private transactionDataService: TransactionDataService,
    private walletTransactionsService: WalletTransactionsService,
    private modalController: ModalController,
    private navController: NavController,
    public submitButtonService: SubmitButtonService,
    private loadingService: LoadingService,
    private route: ActivatedRoute,
    private localNotificationsService: LocalNotificationsService,
    private translate: TranslateService,
    private alertController: AlertController
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.isSending = false;
    this.summaryData = this.transactionDataService.transactionData;
    this.checkMode();
  }

  checkMode() {
    const mode = this.route.snapshot.paramMap.get('mode') === 'retry';
    if (mode) {
      this.canAffordFee().then();
    }
  }

  async askForPassword() {
    const modal = await this.modalController.create({
      component: WalletPasswordComponent,
      cssClass: 'ux-routeroutlet-modal full-screen-modal',
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    return data;
  }

  beginSend() {
    this.askForPassword().then((password: string) => this.send(password));
  }

  private goToSuccess(response: TransactionResponse) {
    this.navController.navigateForward(['/wallets/send/success']).then(() => this.notifyWhenTransactionMined(response));
  }

  private send(password: string) {
    this.loadingService.show().then(() => {
      if (this.summaryData.balance >= this.summaryData.amount) {
        this.walletTransactionsService
          .send(password, this.summaryData.amount, this.summaryData.address, this.summaryData.currency)
          .then((response: TransactionResponse) => this.goToSuccess(response))
          .catch((error) => this.handleSendError(error))
          .finally(() => {
            this.loadingService.dismiss();
            this.isSending = false;
          });
      } else {
        this.loadingService.dismiss();
        this.isSending = false;
        this.navController.navigateForward(['/wallets/send/error/wrong-amount']);
      }
    });
  }

  async canAffordFee() {
    this.isSending = true;
    await this.loadingService.show();
    let cannotAffordFee;

    try {
      cannotAffordFee = await this.walletTransactionsService.canNotAffordFee(this.summaryData);

      await this.loadingService.dismiss();

      if (cannotAffordFee) {
        await this.showAlertNotEnoughNativeToken();
      } else {
        this.beginSend();
      }
    } catch (e) {
      this.isSending = false;
      if (this.isInvalidAddressError(e)) {
        await this.loadingService.dismiss();
        await this.showAlertInvalidAddress();
      } else {
        throw e;
      }
    }
  }

  async showAlert(header: string, message: string, buttonText: string) {
    const alert = await this.alertController.create({
      header: this.translate.instant(header),
      message: this.translate.instant(message),
      cssClass: 'ux-alert-confirm',
      buttons: [
        {
          text: this.translate.instant(buttonText),
          cssClass: 'primary-button',
        },
      ],
    });
    await alert.present();
  }

  async showAlertNotEnoughNativeToken() {
    const route = 'wallets.send.send_summary.alert_not_enough_native_token';
    await this.showAlert(`${route}.title`, `${route}.text`, `${route}.button`);
  }

  async showAlertInvalidAddress() {
    const route = 'wallets.send.send_summary.alert_invalid_address';
    await this.showAlert(`${route}.title`, `${route}.text`, `${route}.button`);
  }

  private createNotification(transaction: TransactionReceipt): LocalNotificationSchema[] {
    return [
      {
        id: 1,
        title: this.translate.instant('wallets.send.send_summary.sent_notification.title'),
        body: this.translate.instant('wallets.send.send_summary.sent_notification.body', {
          address: transaction.to,
        }),
      },
    ];
  }

  private notifyWhenTransactionMined(response: TransactionResponse) {
    response
      .wait()
      .then((transaction: TransactionReceipt) => this.createNotification(transaction))
      .then((notification: LocalNotificationSchema[]) => this.localNotificationsService.send(notification));
  }

  private handleSendError(error) {
    let url: string;

    if (error.message === 'invalid password') {
      url = '/wallets/send/error/incorrect-password';
    } else if (this.isInvalidAddressError(error)) {
      url = '/wallets/send/error/wrong-address';
    } else if (error.message.startsWith('cannot estimate gas') || error.message.startsWith('insufficient funds')) {
      url = '/wallets/send/error/wrong-amount';
    } else {
      throw error;
    }

    this.navController.navigateForward([url]).then();
  }

  private isInvalidAddressError(error) {
    return (
      error.message.startsWith('provided ENS name resolves to null') ||
      error.message.startsWith('invalid address') ||
      error.message.startsWith('bad address checksum') ||
      error.message.startsWith('resolver or addr is not configured for ENS name')
    );
  }
}

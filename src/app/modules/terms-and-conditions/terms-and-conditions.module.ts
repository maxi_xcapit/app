import { NgModule } from '@angular/core';

import { TermsAndConditionsRoutingModule } from './terms-and-conditions-routing.module';

@NgModule({
  declarations: [],
  imports: [TermsAndConditionsRoutingModule]
})
export class TermsAndConditionsModule {}

import { NgModule } from '@angular/core';

import { TutorialsRoutingModule } from './tutorials-routing.module';

@NgModule({
  declarations: [],
  imports: [
    TutorialsRoutingModule
  ]
})
export class TutorialsModule { }

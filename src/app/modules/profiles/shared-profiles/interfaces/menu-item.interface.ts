export interface MenuItem {
  name: string;
  text: string;
  route: string;
  type: 'link';
}

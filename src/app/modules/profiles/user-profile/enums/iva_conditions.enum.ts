export enum IvaConditions {
  IVAResponsableInscripto = 'IVA Responsable Inscripto',
  IVAResponsablenoInscripto = 'IVA Responsable no Inscripto',
  IVAnoResponsable = 'IVA no Responsable',
  IVASujetoExento = 'IVA Sujeto Exento',
  ConsumidorFinal = 'Consumidor Final',
  ResponsableMonotributo = 'Responsable Monotributo',
  SujetonoCategorizado = 'Sujeto no Categorizado',
  ProveedordelExterior = 'Proveedor del Exterior',
  ClientedelExterior = 'Cliente del Exterior',
  IVALiberadoLey19640 = 'IVA Liberado – Ley Nº 19.640',
  IVAResponsableInscriptoAgentedePercepcion = 'IVA Responsable Inscripto – Agente de Percepción',
  PequeñoContribuyenteEventual = 'Pequeño Contribuyente Eventual',
  MonotributistaSocial = 'Monotributista Social',
  PequeñoContribuyenteEventualSocial = 'Pequeño Contribuyente Eventual Social'
}

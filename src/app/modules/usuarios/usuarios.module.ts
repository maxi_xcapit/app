import { NgModule } from '@angular/core';

import { UsuariosRoutingModule } from './usuarios-routing.module';

@NgModule({
  declarations: [],
  imports: [
    UsuariosRoutingModule
  ]
})
export class UsuariosModule { }

export const WALLET_TERMS_OPTIONS = [
  {
    title: 'support.wallet-terms-options.title_terms',
    description: '',
    icon: '../../assets/ux-icons/wallet-terms.svg',
    route: '/support/wallet-info/terms',
  },
  {
    title: 'support.wallet-terms-options.title_privacy_politics',
    description: '',
    icon: '../../assets/ux-icons/privacy.svg',
    route: '/support/wallet-info/privacy',
  },
];

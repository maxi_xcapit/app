export const WALLET_PRIVACY = [
  {
    title: 'support.wallet_privacy.question1',
    answer: 'support.wallet_privacy.answer1',
    last: false,
  },
  {
    title: 'support.wallet_privacy.question2',
    answer: 'support.wallet_privacy.answer2',
    last: false,
  },
  {
    title: 'support.wallet_privacy.question3',
    answer: 'support.wallet_privacy.answer3',
    last: false,
  },
  {
    title: 'support.wallet_privacy.question4',
    answer: 'support.wallet_privacy.answer4',
    last: false,
  },
  {
    title: 'support.wallet_privacy.question5',
    answer: 'support.wallet_privacy.answer5',
    last: false,
  },
  {
    title: 'support.wallet_privacy.question6',
    answer: 'support.wallet_privacy.answer6',
    last: false,
  },
  {
    title: 'support.wallet_privacy.question7',
    answer: 'support.wallet_privacy.answer7',
    last: false,
  },
  {
    title: 'support.wallet_privacy.question8',
    answer: 'support.wallet_privacy.answer8',
    last: false,
  },
];

export const NFT_TERMS = [
    {
      title: 'support.support_nft.question1',
      answer: 'support.support_nft.answer1',
      last: false,
    },
    {
      title: 'support.support_nft.question2',
      answer: 'support.support_nft.answer2',
      last: false,
    },
    {
      title: 'support.support_nft.question3',
      answer: 'support.support_nft.answer3',
      last: false,
    },
    {
      title: 'support.support_nft.question4',
      answer: 'support.support_nft.answer4',
      last: false,
    },
    {
      title: 'support.support_nft.question5',
      answer: 'support.support_nft.answer5',
      last: false,
    },
    {
      title: 'support.support_nft.question6',
      answer: 'support.support_nft.answer6',
      last: false,
    },
    {
      title: 'support.support_nft.question7',
      answer: 'support.support_nft.answer7',
      last: false,
    },
    {
      title: 'support.support_nft.question8',
      answer: 'support.support_nft.answer8',
      last: false,
    },
    {
      title: 'support.support_nft.question9',
      answer: 'support.support_nft.answer9',
      last: false,
    },
    {
      title: 'support.support_nft.question10',
      answer: 'support.support_nft.answer10',
      last: false,
    },
    {
      title: 'support.support_nft.question11',
      answer: 'support.support_nft.answer11',
      last: true,
    },
  ];
  
export const WALLET_TERMS = [
  {
    title: 'support.wallet_terms.question1',
    answer: 'support.wallet_terms.answer1',
    last: false,
  },
  {
    title: 'support.wallet_terms.question2',
    answer: 'support.wallet_terms.answer2',
    last: false,
  },
  {
    title: 'support.wallet_terms.question3',
    answer: 'support.wallet_terms.answer3',
    last: false,
  },
  {
    title: 'support.wallet_terms.question4',
    answer: 'support.wallet_terms.answer4',
    last: false,
  },
  {
    title: 'support.wallet_terms.question5',
    answer: 'support.wallet_terms.answer5',
    last: false,
  },
  {
    title: 'support.wallet_terms.question6',
    answer: 'support.wallet_terms.answer6',
    last: false,
  },
  {
    title: 'support.wallet_terms.question7',
    answer: 'support.wallet_terms.answer7',
    last: false,
  },
  {
    title: 'support.wallet_terms.question8',
    answer: 'support.wallet_terms.answer8',
    last: false,
  },
  {
    title: 'support.wallet_terms.question9',
    answer: 'support.wallet_terms.answer9',
    last: false,
  },
  {
    title: 'support.wallet_terms.question10',
    answer: 'support.wallet_terms.answer10',
    last: false,
  },
  {
    title: 'support.wallet_terms.question11',
    answer: 'support.wallet_terms.answer11',
    last: false,
  },
  {
    title: 'support.wallet_terms.question12',
    answer: 'support.wallet_terms.answer12',
    last: true,
  },
];

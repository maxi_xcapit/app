export const SUPPORT_OPTIONS = [
  {
    title: 'support.support_options.title_wallet',
    description: 'support.support_options.description_wallet',
    icon: '../../assets/ux-icons/ux-support-wallet.svg',
    route: '/support/wallet',
  },
  {
    title: 'support.support_options.title_nft',
    description: 'support.support_options.description_nft',
    icon: '../../assets/ux-icons/ux-nft.svg',
    route: '/support/nft',
  },
  {
    title: 'support.support_options.title_buy',
    description: 'support.support_options.description_buy',
    icon: '../../assets/ux-icons/ux-buy.svg',
    route: '/support/buy',
  },
  {
    title: 'support.support_options.title_account',
    description: 'support.support_options.description_account',
    icon: '../../assets/ux-icons/ux-xcapit-account.svg',
    route: '/support/account',
  },
  {
    title: 'support.support_options.title_investment',
    description: 'support.support_options.description_investment',
    icon: '../../assets/ux-icons/ux-trending-up.svg',
    route: '/support/binance-investments',
  },
  {
    title: 'support.support_options.title_apikeys',
    description: 'support.support_options.description_apikeys',
    icon: '../../assets/ux-icons/ux-apikeys.svg',
    route: 'support/apikey-binance',
  },
  {
    title: 'support.support_options.title_security',
    description: 'support.support_options.description_security',
    icon: '../../assets/ux-icons/ux-trending-up.svg',
    route: '/support/security',
  },
];

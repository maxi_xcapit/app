export const ABOUT_SECURITY = [
  {
    title: 'support.support_security.question1',
    answer: 'support.support_security.answer1',
    last: false,
  },
  {
    title: 'support.support_security.question2',
    answer: 'support.support_security.answer2',
    last: true,
  },
];

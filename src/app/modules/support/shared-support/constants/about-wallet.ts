export const ABOUT_WALLET = [
  {
    title: 'support.support_wallet.question1',
    answer: 'support.support_wallet.answer1',
    last: false,
  },
  {
    title: 'support.support_wallet.question2',
    answer: 'support.support_wallet.answer2',
    last: false,
  },
  {
    title: 'support.support_wallet.question3',
    answer: 'support.support_wallet.answer3',
    last: false,
  },
  {
    title: 'support.support_wallet.question4',
    answer: 'support.support_wallet.answer4',
    last: false,
  },
  {
    title: 'support.support_wallet.question5',
    answer: 'support.support_wallet.answer5',
    last: false,
  },
  {
    title: 'support.support_wallet.question6',
    answer: 'support.support_wallet.answer6',
    last: false,
  },
  {
    title: 'support.support_wallet.question7',
    answer: 'support.support_wallet.answer7',
    last: false,
  },
  {
    title: 'support.support_wallet.question8',
    answer: 'support.support_wallet.answer8',
    last: false,
  },
  {
    title: 'support.support_wallet.question9',
    answer: 'support.support_wallet.answer9',
    last: false,
  },
  {
    title: 'support.support_wallet.question10',
    answer: 'support.support_wallet.answer10',
    last: false,
  },
  {
    title: 'support.support_wallet.question11',
    answer: 'support.support_wallet.answer11',
    last: false,
  },
  {
    title: 'support.support_wallet.question12',
    answer: 'support.support_wallet.answer12',
    last: false,
  },
  {
    title: 'support.support_wallet.question13',
    answer: 'support.support_wallet.answer13',
    last: false,
  },
  {
    title: 'support.support_wallet.question14',
    answer: 'support.support_wallet.answer14',
    last: false,
  },
  {
    title: 'support.support_wallet.question15',
    answer: 'support.support_wallet.answer15',
    last: false,
  },
  {
    title: 'support.support_wallet.question16',
    answer: 'support.support_wallet.answer16',
    last:false,
  },
  {
    title: 'support.support_wallet.question17',
    answer: 'support.support_wallet.answer17',
    last:false,
  },
    {
    title: 'support.support_wallet.question18',
    answer: 'support.support_wallet.answer18',
    last:true,
  },
];

export const ABOUT_BINANCE = [
  {
    title: 'support.support_binance.question1',
    answer: 'support.support_binance.answer1',
    last: false,
  },
  {
    title: 'support.support_binance.question2',
    answer: 'support.support_binance.answer2',
    last: false,
  },
  {
    title: 'support.support_binance.question3',
    answer: 'support.support_binance.answer3',
    last: false,
  },
  {
    title: 'support.support_binance.question4',
    answer: 'support.support_binance.answer4',
    last: false,
  },
  {
    title: 'support.support_binance.question5',
    answer: 'support.support_binance.answer5',
    last: false,
  },
  {
    title: 'support.support_binance.question6',
    answer: 'support.support_binance.answer6',
    last: true,
  },
];

import { NgModule } from '@angular/core';

import { ReferralsRoutingModule } from './referrals-routing.module';

@NgModule({
  declarations: [],
  imports: [ReferralsRoutingModule]
})
export class ReferralsModule {}

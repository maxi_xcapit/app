import { NgModule } from '@angular/core';

import { SubscriptionsRoutingModule } from './subscriptions-routing.module';

@NgModule({
  declarations: [],
  imports: [
    SubscriptionsRoutingModule
  ]
})
export class SubscriptionsModule { }

import { NgModule } from '@angular/core';

import { DepositAddressesRoutingModule } from './deposit-addresses-routing.module';

@NgModule({
  declarations: [],
  imports: [
    DepositAddressesRoutingModule
  ]
})
export class DepositAddressesModule { }

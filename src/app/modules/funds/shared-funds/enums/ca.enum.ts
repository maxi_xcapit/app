export enum CA {
  BTC = 'BTC',
  USDT = 'USDT',
  BNB = 'BNB',
  ETH = 'ETH',
  LTC = 'LTC'
}

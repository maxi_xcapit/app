export enum FundFormActions {
  EditProfitLoss = 'edit',
  NewFund = 'new',
  RenewFund = 'renew'
}

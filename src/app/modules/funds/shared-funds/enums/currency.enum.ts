export enum Currency {
  USDT = 'USDT',
  BTC = 'BTC',
  USD = 'USD'
}

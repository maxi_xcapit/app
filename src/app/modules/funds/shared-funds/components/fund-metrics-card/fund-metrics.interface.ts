export interface FundMetricsInterface {
  cumulative_return: number;
  max_drawdown: number;
  longest_drawdown: number;
  sharpe: number;
}

import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Currency } from '../../enums/currency.enum';
import { ApiFundsService } from '../../services/api-funds/api-funds.service';
import { FundBalanceDetailComponent } from '../fund-balance-detail/fund-balance-detail.component';
import { LocalStorageService } from 'src/app/shared/services/local-storage/local-storage.service';

@Component({
  selector: 'app-fund-portfolio-card',
  template: `
    <div class="fpc" (click)="this.viewDetails()">
      <div class="fpc__content ion-padding">
        <div class="fpc__content__left">
          <app-fund-balance-chart
            [fundBalance]="this.orderedPortfolio"
            [currency]="this.currencyBase"
          ></app-fund-balance-chart>
        </div>
        <div class="fpc__content__right">
          <div class="by-currency" *ngIf="this.orderedPortfolio">
            <div class="by-currency__item" *ngFor="let p of this.orderedPortfolio | slice: 0:3">
              <div class="by-currency__item__left">
                <div class="color" [style.background-color]="p.color"></div>
                <ion-text class="semibold text ux-font-text-xs" color="uxdark">{{ p.ca }} </ion-text>
              </div>
              <div>
                <ion-text class="semibold text ux-font-text-xs" color="uxdark"
                  >{{ p.percentage | number: '1.0-2' }} %</ion-text
                >
              </div>
            </div>
          </div>
          <div class="base">
            <ion-text class="ux-font-text-xl" color="uxdark">
              {{
                this.totalBase
                  | currencyFormat
                    : {
                        currency: this.currencyBase,
                        formatBTC: '1.2-6',
                        formatUSDT: '1.2-2'
                      }
                  | hideText: this.hideFundText
              }}
            </ion-text>
          </div>
          <div class="base" style="margin-top:0px">
            <ion-text class="ux-font-text-lg" color="uxdark">
              {{
                this.totalSecond
                  | currencyFormat
                    : {
                        currency: this.currencySecond,
                        formatBTC: '1.2-6',
                        formatUSDT: '1.2-2'
                      }
                  | hideText: this.hideFundText
              }}
            </ion-text>
          </div>
        </div>
      </div>
      <div class="fpc__footer">
        <ion-button
          appTrackClick
          name="View Details"
          fill="clear"
          size="small"
          class="fpc__footer__details-button ux-font-button-small"
        >
          {{ 'funds.fund_detail.fund_portfolio_card.view_detail' | translate }}
          <ion-icon slot="end" color="info" name="ux-forward"></ion-icon>
        </ion-button>
      </div>
    </div>
  `,
  styleUrls: ['./fund-portfolio-card.component.scss'],
})
export class FundPortfolioCardComponent implements OnInit {
  @Input() fundBalance: any;
  @Input() fundName: string;
  @Input() isOwner: any;
  hideFundText: boolean;

  orderedPortfolio: Array<{
    ca: string;
    amount: number;
    value: number;
    percentage: number;
    color: string;
  }>;
  currencyBase: string;
  currencySecond: string;
  totalBase: number;
  totalSecond: number;

  currencies = [Currency.BTC, Currency.USDT];

  constructor(
    private modalController: ModalController,
    private localStorageService: LocalStorageService,
    private translate: TranslateService,
    private apiFunds: ApiFundsService
  ) {}

  ngOnInit() {
    this.orderChartData();
    this.setTotals();
    this.setCurrency();
    this.subscribeOnHideFunds();
  }

  subscribeOnHideFunds() {
    this.localStorageService.hideFunds.subscribe((res) => (this.hideFundText = res));
  }

  setTotals() {
    this.totalBase = this.fundBalance.balance.end_balance;
    this.totalSecond = this.fundBalance.balance.to_ca.end_balance;
  }

  setCurrency() {
    this.currencyBase = this.fundBalance.fund.currency;
    if (this.currencyBase === Currency.BTC) {
      this.currencySecond = Currency.USDT;
    } else {
      this.currencySecond = Currency.BTC;
    }
  }

  orderChartData() {
    this.orderedPortfolio = this.fundBalance.balance.summary.sort((a: any, b: any) =>
      a.percentage < b.percentage ? 1 : a.percentage > b.percentage ? -1 : 0
    );
  }

  async viewDetails() {
    const modal = await this.modalController.create({
      component: FundBalanceDetailComponent,
      componentProps: {
        orderedPortfolio: this.orderedPortfolio,
        startDate: this.fundBalance.balance.start_time,
        endDate: this.fundBalance.balance.end_time,
        currency: this.fundBalance.fund.currency,
        fundName: this.fundName,
        isOwner: this.isOwner,
      },
      swipeToClose: false,
      cssClass: 'ux-routeroutlet-modal full-screen-modal',
    });

    await modal.present();
  }
}

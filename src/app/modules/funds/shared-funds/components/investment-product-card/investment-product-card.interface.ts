export interface InvestmentProductInterface {
  profile: string;
  min_capital: string;
  annual_interest?: string;
  percentage?: string;
  link_info: string;
  risk: number;
  currency: string;
}

export interface FundPercentageEvolutionChartInterface {
  take_profit: number;
  stop_loss: number;
  percentage_evolution: number[];
  timestamp: any[];
}

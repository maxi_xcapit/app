export enum PlanType {
  free = 'free',
  paid = 'paid',
  premium = 'premium',
}

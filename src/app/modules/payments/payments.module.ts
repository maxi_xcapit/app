import { NgModule } from '@angular/core';
import { PaymentsRoutingModule } from './payments-routing.module';

@NgModule({
  declarations: [],
  imports: [PaymentsRoutingModule]
})
export class PaymentsModule {}

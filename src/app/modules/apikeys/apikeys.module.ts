import { NgModule } from '@angular/core';

import { ApikeysRoutingModule } from './apikeys-routing.module';



@NgModule({
  declarations: [],
  imports: [
    ApikeysRoutingModule
  ]
})
export class ApikeysModule { }

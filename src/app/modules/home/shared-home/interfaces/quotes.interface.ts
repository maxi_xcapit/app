export interface Quotes {
  symbol: string;
  lastPrice: number;
  priceChangePercent: number;
}

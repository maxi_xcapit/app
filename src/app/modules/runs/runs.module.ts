import { NgModule } from '@angular/core';
import { RunsRoutingModule } from './runs-routing.module';

@NgModule({
  declarations: [],
  imports: [
    RunsRoutingModule
  ]
})
export class RunsModule { }

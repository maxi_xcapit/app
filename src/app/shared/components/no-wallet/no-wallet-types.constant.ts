export const NO_WALLET_TYPES = {
  no_wallet_fiat_ramps: {
    urlClose: '/tabs/home',
    urlPage: '/tabs/wallets',
    img: 'assets/img/wallets/no-wallet.svg',
    title: 'wallets.no_wallet.title',
    subtitle: 'wallets.no_wallet.subtitle',
    nameButton: 'wallets.no_wallet.button',
  },
  no_wallet_defi_investments: {
    urlClose: '/tabs/home',
    urlPage: '/tabs/wallets',
    urlLink: '/tabs/investments/defi',
    img: 'assets/img/wealth_management/no-wallet.svg',
    title: 'wealth_managements.no_wallet.title',
    subtitle: 'wealth_managements.no_wallet.subtitle',
    nameButton: 'wealth_managements.no_wallet.button',
    nameLink: 'wealth_managements.no_wallet.link',
  },
};
